/**
 * Mortem
 * Morbid stats
 *
 * @author Josh Avanier
 * @license MIT
 */

function Mortem (d,ley) {
    this.d = d;
    this.ley = ley;
    this.led = ley*365 + ley/4;
    this.ndl = Math.round((new Date() - d)/864E5); 
    this.etr = Math.round(this.led - this.ndl);
    this.pro = this.ndl / this.led * 100;
    this.eyd = d.getFullYear() + ley;
}
 
